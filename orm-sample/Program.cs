﻿using System;
using orm_sample.Config;
using orm_sample.Main;

namespace orm_sample
{
    /// <summary>
    /// エントリクラス
    /// </summary>
    class Program
    {
        /// <summary>
        /// エントリポイント
        /// </summary>
        /// <param name="args">引数</param>
        static void Main(string[] args)
        {
            var config = AppSettingUtil.GetAppConfig();
            var type = args.Length != 0 ? args[0] : "";

            switch (type)
            {
                case "0":

                    break;
                default:
                    new NoOrmMain(config.ConnectionStrings[0].ConfigConnection).Execute();
                    break;
            }
        }
    }
}
