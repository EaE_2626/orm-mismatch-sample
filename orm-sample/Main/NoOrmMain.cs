﻿using impedance_mismatch.Context;
using impedance_mismatch.Dao;
using System.Linq;
using System;
using System.Collections.Generic;

namespace orm_sample.Main
{
    /// <summary>
    /// ORM未使用クラス
    /// </summary>
    public class NoOrmMain
    {
        /// <summary> 接続文字列 </summary>
        private string connectionString;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="connectionString">接続文字列</param>
        public NoOrmMain(string connectionString)
        {

            this.connectionString = connectionString;
        }

        /// <summary>
        /// 実行
        /// </summary>
        public void Execute()
        {
            using (var context = new DbContext(connectionString))
            {
                /* O/Rマッパーを使う、使わないに限らず、プログラミングで難しいのは汎用的で再利用性の高いコードをどうやって書くか。
                 * 複雑さを極限まで排除して、どこまでシンプル差を追求できるか。
                 * プロジェクト全体を通してのポリシーやアーキテクトがが分かりやすい構造にできるか。
                 */ 

                /*  Daoを介したテーブル単体のレコード取得
                 *  リレーションは意識せずに単体テーブルのレコード取得にだけ意識を向ければいいため、階層構造などは何も関係がない。
                 *  取得した値と、対象クラスのプロパティの突合せを行えば値を格納することは比較的簡単。
                 */
                var userAllSingle = context.USER.Select().ToList();
                var bbAllSingle = context.BB_CTRL.Select().ToList();
                var cnt = 1;
                userAllSingle.ForEach(u =>
                {
                    Console.WriteLine($"==== Record : {cnt}========");
                    Console.WriteLine(u.USER_ID);
                    Console.WriteLine(u.USER_NAME);
                    Console.WriteLine($"=======================");
                    Console.WriteLine();
                    cnt++;
                });

                cnt = 1;
                bbAllSingle.ForEach(b =>
                {
                    var t = b.GetType();
                    Console.WriteLine($"==== Record : {cnt}========");
                    foreach (var prop in t.GetProperties())
                    {
                        Console.WriteLine(t.GetProperty(prop.Name).GetValue(b));
                    }
                    Console.WriteLine($"=======================");
                    Console.WriteLine();
                    cnt++;
                });



                /* テーブルの結合(リレーション)を表現しようとすると、途端にOOPで表現することが難しくなってしまう。
                 * SQLを発行して、DBから結果を取得した場合、渡される情報をDaoに当て込むことはかなり難しい。
                 * 取得結果に対して、明示的にカラム名を指定して値を取得していかなければならず、汎用的なメソッドにするにはコストが大きい。
                 * 汎用的にしない場合には、結合の方法や取得の方法によって、それごとにメソッドを作成する必要がある。
                 * 
                 * そして、結合後のデータ表現の仕方もOOP上では問題になる。
                 * 結合した結果をOOPのプログラム上でどのように表現するのか？？？
                 */
                var joinSql = @"
SELECT 
	A.USER_ID,
	A.USER_NAME,
	B.BB_ID,
	B.CREATE_TIME,
	B.BB_TITLE,
	B.BB_BODY
FROM 
	sample.USER AS A LEFT OUTER JOIN sample.BB_CTRL 
	AS B ON A.USER_ID = B.USER_ID 
WHERE 
	A.USER_ID = 1
ORDER BY 
    BB_ID";

                var joinResult = context.SqlQueryRead(joinSql);
                cnt = 1;
                while (joinResult.Read())
                {
                    Console.WriteLine($"==== Record : {cnt}========");
                    Console.WriteLine(joinResult["USER_ID"]);
                    Console.WriteLine(joinResult["USER_NAME"]);
                    Console.WriteLine(joinResult["BB_ID"]);
                    Console.WriteLine(joinResult["CREATE_TIME"]);
                    Console.WriteLine(joinResult["BB_TITLE"]);
                    Console.WriteLine(joinResult["BB_BODY"]);
                    Console.WriteLine($"=======================");
                    cnt++;
                }
                joinResult.Close();


                /*
                 * Join結果をOOPのプログラム上確保しておく方法はいくつか存在するが、とれる方法には限りがある。
                 * ① Join対象となったテーブルを複数管理する包括的なエンティティを作成する。
                 * ② 対象のカラム名を内包する匿名クラスにする。
                 * ③ それぞれのエンティティでRelastionshipを再現する。
                 * ...etcの方法がある。
                 */

                // この方法だと、結合の種類に応じて膨大な包括エンティティができる。
                var joinResultPattern_First = Pattern_First(context, joinSql);

                // 匿名クラスの場合、型が存在しないため、エンティティ自身がプロパティを明示的に表現できない。
                // Reflectionもしくは、dynamicによる動的アクセスをここみるしかない。(性能との闘い。)
                var joinResultPattern_Second = Pattern_Second(context, joinSql);

                // Entityが関連性を表しているため比較的理解はしやすいが、contextからのアクセスに限定するという
                // ポリシーの元でこの関係性を表現しようとする恐ろしいほど難易度の高い実装になる。
                var joinResultPattern_Third = Pattern_Third(context, joinSql);


                // どのポリシーやアーキテクトを採用することが、息の長いプログラムになるのかを開発当初から考えなければならない。
            }
        }

        /// <summary>
        /// ① Join対象となったテーブルを複数管理する包括的なエンティティを作成する。
        /// </summary>
        /// <param name="context">DBコンテキスト</param>
        /// <param name="sql">Join用SQL</param>
        /// <returns>包括エンティティ</returns>
        private GenericObject Pattern_First(DbContext context, string sql)
        {
            var go = new GenericObject();
            var result = context.SqlQueryRead(sql);
            while (result.Read())
            {
                var u = new USER();
                u.USER_ID = Convert.ToInt32(result["USER_ID"]);
                u.USER_NAME = Convert.ToString(result["USER_NAME"]);

                var b = new BB_CTRL();
                b.BB_ID = Convert.ToInt32(result["BB_ID"]);
                b.USER_ID = Convert.ToInt32(result["USER_ID"]);
                b.CREATE_TIME = Convert.ToDateTime(result["CREATE_TIME"]);
                b.BB_TITLE = Convert.ToString(result["BB_TITLE"]);
                b.BB_BODY = Convert.ToString(result["BB_BODY"]);

                go.USER.Add(u);
                go.BB_CTRL.Add(b);
            }
            result.Close();

            return go;
        }

        /// <summary>
        /// ② 対象のカラム名を内包する匿名クラスにする。
        /// </summary>
        /// <param name="context">DBコンテキスト</param>
        /// <param name="sql">Join用SQL</param>
        /// <returns>匿名クラス</returns>
        private object Pattern_Second(DbContext context, string sql)
        {
            var ret = new List<object>();
            var result = context.SqlQueryRead(sql);
            while (result.Read())
            {
                ret.Add(new
                {
                    USER_ID = Convert.ToInt32(result["USER_ID"]),
                    USER_NAME = Convert.ToString(result["USER_NAME"]),
                    BB_ID = Convert.ToInt32(result["BB_ID"]),
                    CREATE_TIME = Convert.ToDateTime(result["CREATE_TIME"]),
                    BB_TITLE = Convert.ToString(result["BB_TITLE"]),
                    BB_BODY = Convert.ToString(result["BB_BODY"])
                });
            }
            result.Close();

            return ret;
        }

        /// <summary>
        /// ③ それぞれのエンティティでRelastionshipを再現する。
        /// </summary>
        /// <param name="context">DBコンテキスト</param>
        /// <param name="sql">Join用SQL</param>
        /// <returns>EntityのRelationship</returns>
        private List<USER> Pattern_Third(DbContext context, string sql)
        {
            var ret = new List<USER>();
            var result = context.SqlQueryRead(sql);
            while (result.Read())
            {
                var u = new USER()
                {
                    USER_ID = Convert.ToInt32(result["USER_ID"]),
                    USER_NAME = Convert.ToString(result["USER_NAME"]),
                    BB_CTRL = new List<BB_CTRL>(),
                };

                u.BB_CTRL.Add(new BB_CTRL
                {
                    BB_ID = Convert.ToInt32(result["BB_ID"]),
                    USER_ID = Convert.ToInt32(result["USER_ID"]),
                    CREATE_TIME = Convert.ToDateTime(result["CREATE_TIME"]),
                    BB_TITLE = Convert.ToString(result["BB_TITLE"]),
                    BB_BODY = Convert.ToString(result["BB_BODY"])
                });

                ret.Add(u);
            }
            result.Close();

            return ret;
        }
    }
    
    /// <summary>
    /// 包括エンティティ
    /// </summary>
    class GenericObject
    {
        /// <summary> USERテーブル </summary>
        public List<USER> USER { get; set; } = new List<USER>();
        /// <summary> BB_CTRLテーブル </summary>
        public List<BB_CTRL> BB_CTRL { get; set; } = new List<BB_CTRL>();
    }
}
