﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

namespace orm_sample.Config
{
    /// <summary>
    /// AppSettings クラス
    /// </summary>
    public class AppSettingUtil
    {
        public static AppSettings GetAppConfig()
        {
            using (var sr = new StreamReader("appsettings.json"))
            {
                var str = sr.ReadToEnd();
                return JsonConvert.DeserializeObject<AppSettings>(str);
            }
            
        }
    }

    public class AppSettings
    {
        /// <summary> DB制御情報 </summary>
        public List<ConnectionStrings> ConnectionStrings { get; set; }
    }

    /// <summary>
    /// 接続文字列制御クラス
    /// </summary>
    public class ConnectionStrings
    {
        /// <summary> 接続タイプ </summary>
        public string Type { get; set; }
        /// <summary> 接続文字列 </summary>
        public string ConfigConnection { get; set; }
    }
}
