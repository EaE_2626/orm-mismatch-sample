CREATE TABLE sample.BB_CTRL
(
    BB_ID int not null AUTO_INCREMENT,
    USER_ID int not null,
    CREATE_TIME datetime not null DEFAULT CURRENT_TIMESTAMP,
    BB_TITLE nvarchar(100) not null,
    BB_BODY nvarchar(2000),
    constraint pk_user primary key (BB_ID)
);
