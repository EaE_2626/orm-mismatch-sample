CREATE TABLE sample.USER
(
	USER_ID int not null AUTO_INCREMENT,
	USER_NAME nvarchar(50) not null,
    constraint pk_user primary key (USER_ID)
);