FROM mcr.microsoft.com/dotnet/core/sdk:2.1

# basic setting
RUN apt-get update -qq && apt-get install -y --no-install-recommends && \
    rm -rf /var/lib/apt/lists/*

# create workspace
ENV APP_ROOT /home/workspace
WORKDIR $APP_ROOT
