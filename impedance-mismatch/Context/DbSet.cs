﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Linq;
using impedance_mismatch.Dao;
using impedance_mismatch.Attributes;

namespace impedance_mismatch.Context
{
    /// <summary>
    /// DBレコードセット
    /// </summary>
    /// <typeparam name="T">Dao</typeparam>
    public class DbSet<T>
    {
        /// <summary> SQL接続 </summary>
        private MySqlConnection connection;

        public DbSet(MySqlConnection conn)
        {
            connection = conn;
        }

        /// <summary>
        /// Select
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> Select()
        {
            var sql = string.Format("Select * from {0}", typeof(T).Name);
            var cmd = new MySqlCommand(sql, connection);
            using (var cmdR = cmd.ExecuteReader())
            {
                while (cmdR.Read())
                {
                    var type = typeof(T);
                    var ins = Activator.CreateInstance(type);
                    
                    foreach(var prop in type.GetProperties())
                    {
                        if (prop.GetCustomAttributes(false).Count() > 0)
                            continue;

                        var cast = Convert.ChangeType(cmdR[prop.Name], prop.PropertyType);
                        type.GetProperty(prop.Name).SetValue(ins, cast);
                    }

                    yield return (T)ins;
                }
            }
        }
    }
}
