﻿using impedance_mismatch.Dao;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace impedance_mismatch.Context
{
    public class DbContext : IDisposable
    {
        /// <summary> 接続文字列 </summary>
        public string ConnectionString { get; private set; }

        /// <summary> MySQL コネクション </summary>
        private MySqlConnection connection;

        /// <summary> コンテキスト破棄フラグ </summary>
        private bool disposed = false;

        /// <summary> USER テーブル </summary>
        public DbSet<USER> USER;
        /// <summary> BB_CTRL テーブル </summary>
        public DbSet<BB_CTRL> BB_CTRL;

        /// <summary>
        /// DBコンテキスト
        /// </summary>
        public DbContext(string connectionString)
        {
            ConnectionString = connectionString;
            connection = new MySqlConnection(connectionString);
            connection.Open();

            USER = new DbSet<USER>(connection);
            BB_CTRL = new DbSet<BB_CTRL>(connection);
        }

        /// <summary>
        /// 引数に指定されたQueryを実行し、結果を呼び出し元に返却します
        /// </summary>
        /// <returns>実行結果</returns>
        public MySqlDataReader SqlQueryRead(string sql)
        {
            var cmd = new MySqlCommand(sql, connection);
            return cmd.ExecuteReader();
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            if (!disposed)
            {
                if (connection.State != ConnectionState.Closed)
                    connection.Close();

                if (connection == null)
                    connection = null;

                disposed = true;
            }
        }
    }
}
