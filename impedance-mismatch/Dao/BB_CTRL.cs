﻿using System;

namespace impedance_mismatch.Dao
{
    /// <summary>
    /// 掲示板
    /// </summary>
    public class BB_CTRL
    {
        /// <summary> 掲示板ID </summary>
        public int BB_ID { get; set; }

        /// <summary> ユーザーID </summary>
        public int USER_ID { get; set; }

        /// <summary> 作成日時 </summary>
        public DateTime CREATE_TIME { get; set; }

        /// <summary> 掲示板タイトル </summary>
        public string BB_TITLE { get; set; }

        /// <summary> 掲示板本文 </summary>
        public string BB_BODY { get; set; }
    }
}
