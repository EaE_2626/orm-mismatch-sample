﻿using System.Collections.Generic;
using impedance_mismatch.Attributes;

namespace impedance_mismatch.Dao
{
    /// <summary>
    /// システム利用者ユーザー
    /// </summary>
    public class USER
    {
        /// <summary> ユーザーID </summary>
        public int USER_ID { get; set; }

        /// <summary> ユーザー名 </summary>
        public string USER_NAME { get; set; }

        /* Relationship */
        /// <summary> 掲示板 </summary>
        [Relationship]
        public List<BB_CTRL> BB_CTRL { get; set; }
    }
}
